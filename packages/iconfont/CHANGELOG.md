# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.1.0] - 2019-02-19
### Updated
- Icon glyphs updated for:
    - app-switcher
    - approve
    - check-circle-filled
    - notification-direct
    - search

## [3.0.1] - 2019-02-19
### Fixed
- Installing the package will not also attempt to generate the iconfont.

## [3.0.0] - 2018-07-09
### Added
- Node package extracted from [AUI](https://bitbucket.org/atlassian/aui/).
- ADG2-styled iconography available in distribution in the `atlassian-icons` font files (`ttf`, `woff`, `eot`, and `svg` formats).
- ADG Server-styled iconography available in distribution in the `adgs-icons` font files (`ttf`, `woff`, `eot`, and `svg` formats).
