import './aui.component.layer';
import './aui.component.trigger';
import './styles/aui.pattern.inline-dialog';
export { default as InlineDialogEl } from '@atlassian/aui/src/js/aui/inline-dialog2';
