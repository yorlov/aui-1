/* eslint-env node */
const del = require('del');
const gulp = require('gulp');
const gulpWebserver = require('gulp-webserver');
const argv = require('yargs').argv;

const docsOpts = require('./build/docs.opts');

const buildWebpack = require('./build/docs.webpacker');
const buildMetalsmith = require('./build/docs.metalsmith');

const runWebserver = (opts) => function docsServer () {
    opts = Object.assign({
        host: docsOpts.host,
        port: docsOpts.port,
        open: docsOpts.path
    }, opts);
    return gulp.src('dist')
        .pipe(gulpWebserver(opts));
};

let frontendOpts = {};
let backendOpts = {
    docsVersion: argv.docsVersion
};

const dev = (isDev) => function setDevmode(done) {
    if (isDev) {
        frontendOpts.watch = true;
        backendOpts.watch = true;
    }
    done();
};

const clean = (done) => del(['.tmp', 'dist']).then(() => done());
const build = gulp.series(clean, gulp.parallel(
    function buildFrontend(done) { buildWebpack(frontendOpts)(done) },
    function buildBackend(done) { buildMetalsmith(backendOpts)(done) }
));
const run = gulp.series(dev(false), build, runWebserver({ livereload: false }));
const watch = gulp.series(dev(true), build, runWebserver({ livereload: false }));

module.exports = {
    clean,
    build,
    run,
    watch
};
