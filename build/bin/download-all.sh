#!/bin/bash
set -e

version=$1
if [ -z "${version}" ]; then
    echo "${0} [version]"
    echo ""
    echo "Downloads and extracts the aui plugin, aui-soy, and aui-flat-pack,"
    echo "for the given released version to the current directory."
    exit 1
fi;

if [ ! -d "${version}" ]; then
    mkdir "${version}"
fi
cd "${version}"


base_maven_url='https://packages.atlassian.com/maven'
maven_package_path='/public/com/atlassian/aui'
maven_url=${base_maven_url}${maven_package_path}
curl="curl --silent --fail"

# Maven jars.
for package in auiplugin aui-soy; do
    if [ ! -f "${package}.jar" ]; then
        ${curl} --output "${package}.jar" \
                "${maven_url}/${package}/${version}/${package}-${version}.jar"
    fi
done

# Maven zips.
for package in aui-flat-pack; do
    if [ ! -f "${package}.zip" ]; then
        ${curl} --output "${package}.zip" \
                "${maven_url}/${package}/${version}/${package}-${version}.zip"
    fi
done


for jar in $(ls *.jar); do
    package=$(basename "${jar}" .jar)
    rm -rf ./"${package}"
    mkdir "${package}"
    cd "${package}"
    jar xf "../${jar}"
    cd ..
done

for zip in $(ls *.zip); do
    package=$(basename "${zip}" .zip)
    temp=$(mktemp -d "/tmp/${package}.XXXX")
    unzip -d "${temp}" "${zip}" > /dev/null
    rm -rf ./"${package}"
    mkdir "${package}"
    mv "${temp}"/*/* "${package}"
    rm -rf "${temp}"
done

cd ..
