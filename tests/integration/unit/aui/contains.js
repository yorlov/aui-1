import contains from '@atlassian/aui/src/js/aui/contains';

describe('aui/contains', function () {
    it('globals', function () {
        expect(AJS.contains.toString()).to.equal(contains.toString());
    });
});
