import containDropdown from '@atlassian/aui/src/js/aui/contain-dropdown';

describe('aui/contain-dropdown', function () {
    it('globals', function () {
        expect(AJS.containDropdown.toString()).to.equal(containDropdown.toString());
    });
});
